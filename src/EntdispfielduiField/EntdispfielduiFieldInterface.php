<?php

namespace Drupal\entdispfieldui\EntdispfielduiField;

/**
 * @property int $id
 * @property string $identifier
 * @property string $machine_name
 * @property string $entity_type
 * @property string $bundle
 * @property string $view_mode
 * @property bool|int $visible
 * @property int $weight
 * @property string $plugin_id
 * @property mixed $plugin_options
 */
interface EntdispfielduiFieldInterface {

}
