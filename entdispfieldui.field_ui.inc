<?php
use Drupal\cfrapi\SummaryBuilder\SummaryBuilder_Static;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param array $form
 * @param array $form_state
 *
 * @see field_group_form_field_ui_field_overview_form_alter()
 * @see field_group_field_ui_overview_form_alter()
 */
function entdispfieldui_form_field_ui_display_overview_form_alter(array &$form, array &$form_state) {
  # dpm(__FUNCTION__);

  // Only start altering the form if we need to.
  if (empty($form['#fields']) && empty($form['#extra'])) {
    return;
  }

  $params = _entdispfieldui_field_ui_form_params($form);

  $form['#entdispfieldui_field_names'] = array_keys($params->entdispfieldui_fields);

  $table = &$form['fields'];

  // Add a region for 'add_new' rows, but only when fields are available and
  // thus regions.
  // This is the same as in field_group.
  if (isset($table['#regions'])) {
    $table['#regions'] += [
      'add_new' => ['title' => '&nbsp;'],
    ];
  }

  $refresh_rows = NULL;
  if (isset($form_state['values']['refresh_rows'])) {
    $refresh_rows = $form_state['values']['refresh_rows'];
  }
  elseif (isset($form_state['input']['refresh_rows'])) {
    $refresh_rows = $form_state['input']['refresh_rows'];
  }

  // Create the entdisp rows and check actions.
  foreach (array_keys($params->entdispfieldui_fields) as $name) {

    // Play around with form_state so we only need to hold things
    // between requests, until the save button was hit.
    if (isset($form_state['entdispfieldui'][$name])) {
      // @todo Why is this by-reference?
      $entdispfieldui_field_ref =& $form_state['entdispfieldui'][$name];
    }
    else {
      // @todo Why is this by-reference?
      $entdispfieldui_field_ref =& $params->entdispfieldui_fields[$name];
    }

    // Check the currently selected formatter, and merge persisted values for
    // formatter settings for the entdisp field.
    // This needs to be done first, so all fields are updated before creating form elements.
    if (NULL !== $refresh_rows && (string)$refresh_rows === (string)$name) {
      $settings = NULL;
      /** @noinspection UnSafeIsSetOverArrayInspection */
      if (isset($form_state['values']['fields'][$name])) {
        $settings = $form_state['values']['fields'][$name];
      }
      /** @noinspection UnSafeIsSetOverArrayInspection */
      elseif (isset($form_state['input']['fields'][$name])) {
        $settings = $form_state['input']['fields'][$name];
      }

      if (array_key_exists('settings_edit', $settings)) {
        $entdispfieldui_field_ref = $form_state['entdispfieldui'][$name];
      }
    }
    // Save the field when the configuration is submitted.
    if (0
      || !empty($form_state['values'][$name . '_formatter_settings_update'])
      || !empty($form_state['values']['fields'][$name]['entdisp_settings'])
    ) {
      _entdispfieldui_formatter_settings_update($entdispfieldui_field_ref, $form_state['values']['fields'][$name]);
    }
    // After all updates are finished, let the form_state know.
    $form_state['entdispfieldui'][$name] = $entdispfieldui_field_ref;

    $id = str_replace('_', '-', $name);
    $js_rows_data[$id] = ['type' => 'entdispfieldui_field', 'name' => $name];

    $table[$name] = _entdispfieldui_build_table_row(
      $params,
      $entdispfieldui_field_ref,
      $name,
      (string)$form_state['formatter_settings_edit'] === (string)$name);

    $table[$name]['parent_wrapper']['parent']['#options'] = $table['#parent_options'];
    $table[$name]['#attributes']['id'] = $id;
  }

  // Additional row: add new EntDisP field.
  $table['_add_new_field'] = _entdispfieldui_build_new_row('_add_new_field', $table['#parent_options'], $params);

  # $form['#attached']['css'][] = drupal_get_path('module', 'entdispfieldui') . '/entdispfieldui.field_ui.css';
  # $form['#attached']['js'][] = drupal_get_path('module', 'entdispfieldui') . '/entdispfieldui.field_ui.js';

  /* @see _entdispfieldui_field_overview_validate() */
  $form['#validate'][] = '_entdispfieldui_field_overview_validate';
  /* @see _entdispfieldui_field_overview_submit_prepare() */
  $form['#submit'] = array_merge(['_entdispfieldui_field_overview_submit_prepare'], $form['#submit']);
  /* @see _entdispfieldui_field_overview_submit() */
  $form['#submit'][] = '_entdispfieldui_field_overview_submit';

  # dpm($form['#submit'], __FUNCTION__);
  # dpm($table);

  # _renderpolice_check_element($form);

  // @todo Create vertical tab for clone operation.
  # field_group_field_ui_create_vertical_tabs($form, $form_state, $params);
}

/**
 * @param object $params
 * @param object $entdispfieldui_field
 * @param string $name
 * @param bool $isOpen
 *
 * @return array
 */
function _entdispfieldui_build_table_row($params, $entdispfieldui_field, $name, $isOpen) {

  $tableRow = [
    '#attributes' => ['class' => ['draggable', 'tabledrag-leaf']],
    '#row_type' => 'extra_field', // 'entdispfieldui_field',
    '#region_callback' => 'field_ui_display_overview_row_region',  // $params->region_callback
    '#js_settings' => ['rowHandler' => 'field'],
    'human_name' => [
      '#markup' => check_plain($entdispfieldui_field->machine_name),
    ],
    'weight' => [
      '#type' => 'textfield',
      '#title' => t('Weight for @title', ['@title' => $name]),
      '#title_display' => 'invisible',
      '#default_value' => $entdispfieldui_field->weight,
      '#size' => 3,
      '#attributes' => ['class' => ['field-weight']],
    ],
    'parent_wrapper' => [
      'parent' => [
        '#type' => 'select',
        '#title' => t('Parent for @title', ['@title' => $name]),
        '#title_display' => 'invisible',
        # '#options' => $table['#parent_options'],
        '#empty_value' => '',
        '#attributes' => ['class' => ['field-parent']],
        '#parents' => ['fields', $name, 'parent'],
      ],
      'hidden_name' => [
        '#type' => 'hidden',
        '#default_value' => $name,
        '#attributes' => ['class' => ['field-name']],
      ],
    ],
    'machine_name' => [
      '#markup' => check_plain($entdispfieldui_field->machine_name),
    ],
    'format' => [
      'type' => [
        '#type' => 'select',
        '#title' => t('Visibility for @title', ['@title' => $name]),
        '#title_display' => 'invisible',
        '#options' => [
          'visible' => t('Visible'),
          'hidden' => t('Hidden'),
        ],
        '#default_value' => $entdispfieldui_field->visible ? 'visible' : 'hidden',
        '#parents' => ['fields', $name, 'type'],
        '#attributes' => ['class' => ['field-formatter-type']],
      ],
    ],
  ];

  // Base button element for the various formatter settings actions.
  $base_button = [
    '#submit' => ['field_ui_display_overview_multistep_submit'],
    '#ajax' => [
      'callback' => 'field_ui_display_overview_multistep_js',
      'wrapper' => 'field-display-overview-wrapper',
      'effect' => 'fade',
    ],
    '#field_name' => $name,
  ];

  if ($isOpen) {
    // We are currently editing this field's formatter settings. Display the
    // settings form and submit buttons.
    $tableRow['format']['#cell_attributes'] = ['colspan' => 3];
    $tableRow['format']['settings_edit_form'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['field-formatter-settings-edit-form']],
      '#parents' => ['fields', $name, 'entdisp_settings'],
      '#weight' => -5,
      // Create a settings form where hooks can pick in.
      'settings' => _entdispfieldui_plugin_settings_form($entdispfieldui_field),
      'actions' => [
        '#type' => 'actions',
        'save_settings' => $base_button + [
            '#type' => 'submit',
            '#name' => $name . '_formatter_settings_update',
            '#value' => t('Update'),
            '#op' => 'update',
          ],
        'cancel_settings' => $base_button + [
            '#type' => 'submit',
            '#name' => $name . '_formatter_settings_cancel',
            '#value' => t('Cancel'),
            '#op' => 'cancel',
            // Do not check errors for the 'Cancel' button.
            '#limit_validation_errors' => [],
          ],
      ],
    ];
    $tableRow['#attributes']['class'][] = 'field-formatter-settings-editing';

    unset($tableRow['format']['#markup']);
  }
  else {
    // After saving, the settings are updated here as well. First we create
    // the element for the table cell.
    $tableRow['settings_summary'] = ['#markup' => ''];
    /** @noinspection IsEmptyFunctionUsageInspection */
    if (!empty($entdispfieldui_field->plugin_id)) {
      $tableRow['settings_summary'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['field-formatter-summary']],
        [
          '#markup' => _entdispfieldui_plugin_settings_summary($entdispfieldui_field)
        ],
      ];
    }

    // Add the configure button.
    $tableRow['settings_edit'] = $base_button + [
        '#type' => 'image_button',
        '#name' => $name . '_entdispfieldui_settings_edit',
        '#src' => 'misc/configure.png',
        '#attributes' => ['class' => ['field-formatter-settings-edit'], 'alt' => t('Edit')],
        '#op' => 'edit',
        // Do not check errors for the 'Edit' button.
        '#limit_validation_errors' => [],
        '#prefix' => '<div class="field-formatter-settings-edit-wrapper">',
        '#suffix' => '</div>',
      ];

    $tableRow['settings_edit']['#suffix'] .= l(
      t('delete'),
      // @todo Define route '*/entdispfieldui/%/delete/*' with hook_menu().
      $params->admin_path . '/entdispfieldui/' . $params->view_mode . '/' . $name . '/delete');
  }

  return $tableRow;
}

/**
 * Helper function to add a row in the overview forms.
 *
 * @param string $name
 * @param string[] $parent_options
 * @param object $params
 *
 * @return array
 *
 * @see field_group_add_row()
 */
function _entdispfieldui_build_new_row($name, $parent_options, $params) {
  return [
    '#attributes' => ['class' => ['draggable', 'tabledrag-leaf', 'add-new']],
    # '#row_type' => 'add_new_entdispfieldui',
    '#row_type' => 'extra_field',
    // @todo Is this a valid row handler?
    '#js_settings' => ['rowHandler' => 'field'],
    '#region_callback' => $params->region_callback,
    'label' => [
      '#markup' => '<label>' . t('Add new EntDisP field') . '</label>',
    ],
    'weight' => [
      '#type' => 'textfield',
      '#default_value' => field_info_max_weight($params->entity_type, $params->bundle, $params->view_mode) + 3,
      '#size' => 3,
      '#title_display' => 'invisible',
      '#title' => t('Weight for new EntDisP field'),
      '#attributes' => ['class' => ['field-weight']],
      '#pre_render' => ['_entdispfieldui_cell_placeholder_pre_render'],
      '#post_render' => ['_entdispfieldui_cell_placeholder_post_render'],
      '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
    ],
    'parent_wrapper' => [
      'parent' => [
        '#title_display' => 'invisible',
        '#title' => t('Parent for new EntDisP field'),
        '#type' => 'select',
        '#options' => $parent_options,
        '#empty_value' => '',
        '#attributes' => ['class' => ['field-parent']],
        '#parents' => ['fields', $name, 'parent'],
      ],
      'hidden_name' => [
        '#type' => 'hidden',
        '#default_value' => $name,
        '#attributes' => ['class' => ['field-name']],
      ],
    ],
    'machine_name' => [
      '#type' => 'textfield',
      '#title_display' => 'invisible',
      '#title' => t('Machine name for new EntDisP field'),
      // This field should stay LTR even for RTL languages.
      '#field_prefix' => '<span dir="ltr">entdisp_',
      '#field_suffix' => '</span>&lrm;',
      '#attributes' => ['dir' => 'ltr'],
      '#size' => 15,
      '#description' => t('Machine name (a-z, 0-9, _)'),
      '#cell_attributes' => ['colspan' => 1],
    ],
    'format' => [
      'type' => [
        '#type' => 'select',
        '#title' => t('Visibility for @title', ['@title' => $name]),
        '#title_display' => 'invisible',
        '#options' => [
          'visible' => t('Visible'),
          'hidden' => t('Hidden'),
        ],
        '#default_value' => 'hidden',
        '#parents' => ['fields', $name, 'type'],
        '#attributes' => ['class' => ['field-formatter-type']],
      ],
    ],
    'settings_summary' => [],
    'settings_edit' => [],
  ];
}

/**
 * Creates a settings form for entdispfieldui fields.
 *
 * @param object $entdispfieldui_field
 *
 * @return array
 *
 * @see field_group_format_settings_form()
 */
function _entdispfieldui_plugin_settings_form($entdispfieldui_field) {

  $settings = [
    'id' => $entdispfieldui_field->plugin_id,
    'options' => $entdispfieldui_field->plugin_options,
  ];

  return [
    /* @see entdisp_element_info() */
    '#type' => 'entdisp',
    '#default_value' => $settings,
    '#entity_type' => $entdispfieldui_field->entity_type,
    '#bundle' => $entdispfieldui_field->bundle,
  ];
}

/**
 * Creates a settings form for entdispfieldui fields.
 *
 * @param object $entdispfieldui_field
 *
 * @return array
 *
 * @see field_group_format_settings_summary()
 */
function _entdispfieldui_plugin_settings_summary($entdispfieldui_field) {

  $settings = [
    'id' => $entdispfieldui_field->plugin_id,
    'options' => $entdispfieldui_field->plugin_options,
  ];

  return entdisp()
    ->etBundleGetDisplayManager($entdispfieldui_field->entity_type, $entdispfieldui_field->bundle)
    ->confGetSummary($settings, new SummaryBuilder_Static());
}

/**
 * Validate the label. Label is required for fieldsets that are collapsible.
 *
 * @param array $element
 * @param array $form_state
 *
 * @see field_group_format_settings_label_validate()
 */
/* function _entdispfieldui_format_settings_label_validate($element, &$form_state) {
  $entdispfieldui_row = $form_state['values']['fields'][$element['#parents'][1]];
  $settings = $entdispfieldui_row['format_settings']['settings'];
  $name = $form_state['formatter_settings_edit'];
  // No idea why this is needed..
  $form_state['values']['fields'][$name]['settings_edit_form']['settings'] = $settings;
} */

/**
 * Helper function to get the form parameters to use while
 * building the fields and display overview form.
 *
 * @param array $form
 *
 * @return \stdClass
 *
 * @see field_group_field_ui_form_params()
 */
function _entdispfieldui_field_ui_form_params(array &$form) {

  $params = new stdClass();
  $params->entity_type = $form['#entity_type'];
  $params->bundle = $form['#bundle'];
  $params->admin_path = _field_ui_bundle_admin_path($params->entity_type, $params->bundle);

  /* @see _entdispfieldui_display_overview_row_region() */
  $params->region_callback = '_entdispfieldui_display_overview_row_region';
  $params->view_mode = $form['#view_mode'];

  $params->entdispfieldui_fields = _entdispfieldui_viewmode_fields($params->entity_type, $params->bundle, $params->view_mode, TRUE);

  // Gather parenting data.
  $params->parents = [];

  return $params;
}

/**
 * Returns the region to which a row in the 'Manage display' screen belongs.
 *
 * @param array $row
 *   A field_ui row
 *
 * @return string|null
 *   The current region.
 *
 * @see field_group_display_overview_row_region()
 * @see field_ui_field_overview_row_region()
 */
function _entdispfieldui_display_overview_row_region(array $row) {

  # \Drupal\krumong\dpm($row);

  if (1
    && 'add_new_entdispfieldui' === $row['#row_type']
    && (0
      || !array_key_exists('#value', $row['machine_name'])
      || '' === $row['machine_name']['#value']
    )
  ) {
    return 'add_new';
  }

  if (array_key_exists('#value', $row['format']['type']) && 'visible' === $row['format']['type']['#value']) {
    return 'visible';
  }

  return 'hidden';
}

/**
 * Update handler for entdispfieldui configuration settings.
 *
 * @param object $entdispfieldui_field
 * @param array $settings
 *
 * @see field_group_formatter_settings_update()
 */
function _entdispfieldui_formatter_settings_update($entdispfieldui_field, $settings) {

  $entdisp_settings = !empty($settings['entdisp_settings']['settings'])
    ? $settings['entdisp_settings']['settings']
    : [];

  if (empty($entdisp_settings['id'])) {
    $entdispfieldui_field->plugin_id = NULL;
    $entdispfieldui_field->plugin_options = [];
  }
  elseif (empty($entdisp_settings['options'])) {
    $entdispfieldui_field->plugin_id = $entdisp_settings['id'];
    $entdispfieldui_field->plugin_options = [];
  }
  else {
    $entdispfieldui_field->plugin_id = $entdisp_settings['id'];
    $entdispfieldui_field->plugin_options = $entdisp_settings['options'];
  }
}

/**
 * @param array $form
 * @param array $form_state
 *
 * @see field_group_field_overview_validate()
 */
function _entdispfieldui_field_overview_validate(array $form, array &$form_state) {
  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle_name = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  $entdispfieldui_field_assoc = $form_values['_add_new_field'];

  // Validate if any information was provided in the 'add new EntDisP field' row.
  if (empty($entdispfieldui_field_assoc['machine_name'])) {
    // Nothing to validate.
    return;
  }

  // Missing machine name.
  if (!$entdispfieldui_field_assoc['machine_name']) {
    form_set_error(
      'fields][_add_new_entdispfieldui_field][machine_name',
      t('Add new EntDisP field: you need to provide a machine name.'));
  }
  // Machine name validation.
  else {
    $machine_name = $entdispfieldui_field_assoc['machine_name'];

    // Add the 'entdisp_' prefix.
    if ('entdisp_' !== drupal_substr($machine_name, 0, 6)) {
      $machine_name = 'entdisp_' . $machine_name;
      form_set_value(
        $form['fields']['_add_new_field']['machine_name'],
        $machine_name,
        $form_state);
    }

    // Invalid machine name.
    if (!preg_match('!^entdisp_[a-z0-9_]+$!', $machine_name)) {
      form_set_error(
        'fields][_add_new_entdispfieldui_field][machine_name',
        t(
          'Add new EntDisP field: the machine name %machine_name is invalid. The name must include only lowercase unaccentuated letters, numbers, and underscores.',
          ['%machine_name' => $machine_name]
        ));
    }

    if (drupal_strlen($machine_name) > 32) {
      form_set_error(
        'fields][_add_new_entdispfieldui_field][machine_name',
        t(
          "Add new EntDisP field: the machine name %machine_name is too long. The name is limited to 32 characters, including the 'entdisp_' prefix.",
          ['%machine_name' => $machine_name]
        ));
    }

    if (_entdispfieldui_field_exists($machine_name, $entity_type, $bundle_name, $view_mode)) {
      // Machine name already exists for this entity type / bundle / view mode.
      form_set_error(
        'fields][_add_new_entdispfieldui_field][machine_name',
        t(
          'Add new EntDisP field: the machine name %machine_name already exists.',
          ['%machine_name' => $machine_name]
        ));
    }
  }
}

/**
 * @param array $form
 * @param array $form_state
 */
function _entdispfieldui_field_overview_submit_prepare(
  /** @noinspection PhpUnusedParameterInspection */ array $form,
  array &$form_state
) {
  if (!empty($form_state['values']['fields']['_add_new_field']['machine_name'])) {
    $machine_name = $form_state['values']['fields']['_add_new_field']['machine_name'];
    $form_state['values']['fields'][$machine_name] = $form_state['values']['fields']['_add_new_field'];
  }
}

/**
 * @param array $form
 * @param array $form_state
 *
 * @see field_group_field_overview_submit()
 */
function _entdispfieldui_field_overview_submit(array $form, array &$form_state) {

  # dpm($form, __FUNCTION__);
  # dpm($form_state, '$form_state');
  # dpm($form_state['values']['fields'], '$form_state[values][fields]');

  $form_values = $form_state['values']['fields'];
  $entity_type = $form['#entity_type'];
  $bundle_name = $form['#bundle'];
  $view_mode = $form['#view_mode'];

  // Prepare storage with ctools.
  ctools_include('export');

  // Create new EntDisP field.
  if (!empty($form_values['_add_new_field']['machine_name'])) {

    $values = $form_values['_add_new_field'];

    $new_entdisp_field = (object) [
      'identifier' => $values['machine_name'] . '|' . $entity_type . '|' . $bundle_name . '|' . $view_mode,
      'machine_name' => $values['machine_name'],
      'entity_type' => $entity_type,
      'bundle' => $bundle_name,
      'view_mode' => $view_mode,
      'weight' => $values['weight'],
      'visible' => 'visible' === $values['type'],
      'disabled' => FALSE,
      'plugin_id' => NULL,
      'plugin_options' => [],
    ];

    // Save and enable it in ctools.
    ctools_export_crud_save(ENTDISPFIELDUI_TABLE_NAME, $new_entdisp_field);
    ctools_export_crud_enable(ENTDISPFIELDUI_TABLE_NAME, $new_entdisp_field->identifier);

    // Store new EntDisP field information for any additional submit handlers.
    $form_state['entdispfieldui_fields_added']['_add_new_field'] = $new_entdisp_field->machine_name;

    drupal_set_message(
      t(
        'New EntDisP field %label successfully created.',
        ['%label' => $new_entdisp_field->machine_name]
      ));
  }

  // Update existing EntDisP fields.
  $entdispfieldui_fields = _entdispfieldui_viewmode_fields($entity_type, $bundle_name, $view_mode, TRUE);

  foreach ($form['#entdispfieldui_field_names'] as $machine_name) {

    if (!array_key_exists($machine_name, $entdispfieldui_fields)) {
      continue;
    }

    $entdispfieldui_field = $entdispfieldui_fields[$machine_name];
    $entdispfieldui_field->weight = $form_values[$machine_name]['weight'];

    $entdispfieldui_field->visible = ('visible' === $form_values[$machine_name]['type']) ? 1 : 0;

    if (isset($form_state['entdispfieldui'][$machine_name]->plugin_id)) {
      $entdispfieldui_field->plugin_id = $form_state['entdispfieldui'][$machine_name]->plugin_id;
    }

    if (isset($form_state['entdispfieldui'][$machine_name]->plugin_options)) {
      $entdispfieldui_field->plugin_options = $form_state['entdispfieldui'][$machine_name]->plugin_options;
    }

    ctools_export_crud_save(ENTDISPFIELDUI_TABLE_NAME, $entdispfieldui_field);
  }

  cache_clear_all(ENTDISPFIELDUI_CID_FIELDS_ALL, 'cache_field');
}

function _entdispfieldui_cell_placeholder_pre_render($element) {
  # dpm($element, __FUNCTION__);
  return $element;
}

/**
 * @param string $content
 * @param array $element
 *
 * @return string
 */
function _entdispfieldui_cell_placeholder_post_render(
  $content,
  /** @noinspection PhpUnusedParameterInspection */ array $element
) {
  # dpm($element, __FUNCTION__);
  # dpm($content);
  # dpm(ddebug_backtrace(TRUE));
  return $content;
}
