<?php

/**
 * Implements hook_entity_view().
 *
 * @param object $entity
 * @param string $entity_type
 * @param string $view_mode
 * @param string $langcode
 *
 * @throws \EntityMalformedException
 */
function entdispfieldui_entity_view(
  $entity,
  $entity_type,
  $view_mode,
  /** @noinspection PhpUnusedParameterInspection */ $langcode
) {
  list(,, $bundle) = entity_extract_ids($entity_type, $entity);
  $view_mode_settings = field_view_mode_settings($entity_type, $bundle);
  $actual_mode = !empty($view_mode_settings[$view_mode]['custom_settings'])
    ? $view_mode
    : 'default';
  $entdispfieldui_fields = _entdispfieldui_viewmode_fields($entity_type, $bundle, $actual_mode);
  $manager = entdisp()->etBundleGetDisplayManager($entity_type, $bundle);

  foreach ($entdispfieldui_fields as $entdispfieldui_field) {

    if (!$entdispfieldui_field->visible) {
      continue;
    }

    try {
      $build = $manager
        ->confGetEntityDisplay([
          'id' => $entdispfieldui_field->plugin_id,
          'options' => $entdispfieldui_field->plugin_options,
        ])
        ->buildEntity($entity_type, $entity);
    }
    catch (\Exception $e) {

      $link = $entdispfieldui_field->identifier;
      if (NULL !== $admin_path = _entdispfieldui_viewmode_admin_path($entity_type, $bundle, $actual_mode)) {
        $link = l($link, $admin_path);
      }

      $e_message = $e->getMessage();
      while ($e = $e->getPrevious()) {
        $e_message .= ' / ' . $e->getMessage();
      }

      watchdog('cfrplugin',
        'Broken entity display plugin in view mode item !identifier.'
        . "\n" . 'Exception message: %message',
        [
          '!identifier' => $link,
          '%message' => $e_message,
        ],
        WATCHDOG_WARNING);

      $build = [
        '#markup' => '<!-- broken entity display -->',
      ];
    }

    $entity->content[$entdispfieldui_field->machine_name] = [
      'display' => $build,
      '#weight' => $entdispfieldui_field->weight,
    ];
  }
}
