<?php

/**
 * @param array $form
 * @param array $form_state
 * @param object $obj
 *
 * @return array
 *
 * @see field_group_delete_form()
 */
function entdispfieldui_field_delete_form(
  array $form,
  /** @noinspection PhpUnusedParameterInspection */ array &$form_state,
  $obj
) {
  if (empty($obj->identifier)) {
    return [
      '#markup' => t('No EntDisP field with these parameters.'),
    ];
  }

  $form['#entdispfieldui_field'] = $obj;
  $admin_path = _field_ui_bundle_admin_path($obj->entity_type, $obj->bundle);
  $admin_path .= '/display/' . $obj->view_mode;
  $form['#redirect'] = [$admin_path];

  return confirm_form(
    $form,
    t(
      'Are you sure you want to delete the EntDisP field %name?',
      ['%name' => t($obj->machine_name)]),
    $admin_path,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * Remove from entity type bundle / view mode.
 *
 * @todo we'll have to reset all view mode settings - that will be fun :)
 * (this is a note copied from field_group module)
 *
 * @param array $form
 * @param array $form_state
 *
 * @see field_group_delete_form_submit()
 */
function entdispfieldui_field_delete_form_submit(array $form, array &$form_state) {

  $obj = $form['#entdispfieldui_field'];

  $bundles = field_info_bundles();
  $bundle_label = $bundles[$obj->entity_type][$obj->bundle]['label'];

  ctools_include('export');
  _entdispfieldui_field_export_delete($obj, FALSE);

  drupal_set_message(
    t(
      'The EntDisP field %name has been deleted from %type, view mode %view_mode.',
      [
        '%name' => t($obj->machine_name),
        '%type' => $bundle_label,
        '%view_mode' => $obj->view_mode
      ]
    ));

  // Redirect.
  $form_state['redirect'] = $form['#redirect'];
}
