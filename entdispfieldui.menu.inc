<?php
/**
 * @file
 * Contains hook_menu() and wildcard loaders.
 */

/**
 * Implements hook_menu().
 *
 * @see field_group_menu()
 * @see field_ui_menu()
 */
function entdispfieldui_menu() {

  $items = [];

  foreach (entity_get_info() as $entity_type => $entity_info) {

    if (empty($entity_info['fieldable'])) {
      continue;
    }

    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      if (empty($bundle_info['admin']['path'])) {
        continue;
      }
      // Extract path information from the bundle.
      $path = $bundle_info['admin']['path'];

      // Different bundles can appear on the same path (e.g. %node_type and
      // %comment_node_type). To allow _entdispfieldui_field_load() to extract
      // the actual bundle object from the translated menu router path
      // arguments, we need to identify the argument position of the bundle
      // name string ('bundle argument') and pass that position to the menu
      // loader. The position needs to be casted into a string; otherwise it
      // would be replaced with the bundle name string.
      /** @noinspection UnSafeIsSetOverArrayInspection */
      if (isset($bundle_info['admin']['bundle argument'])) {
        $bundle_arg = $bundle_info['admin']['bundle argument'];
        $bundle_pos = (string) $bundle_arg;
      }
      else {
        $bundle_arg = $bundle_name;
        $bundle_pos = '0';
      }

      // This is the position of the %field_ui_menu placeholder in the
      // items below.
      $field_position = count(explode('/', $path)) + 2;
      $view_mode_pos = $field_position - 1;

      // Extract access information, providing defaults.
      $access
        = array_intersect_key(
          $bundle_info['admin'],
          drupal_map_assoc(['access callback', 'access arguments']))
        + [
          'access callback' => 'user_access',
          'access arguments' => ['administer site configuration'],
        ];

      /* @see entdispfieldui_field_load() */
      $items["$path/entdispfieldui/%/%entdispfieldui_field/delete"]
        = [
          'load arguments' => [$entity_type, $bundle_arg, $bundle_pos, $view_mode_pos, '%map'],
          'title' => 'Delete',
          'page callback' => 'drupal_get_form',
          'page arguments' => ['entdispfieldui_field_delete_form', $field_position],
          'type' => MENU_CALLBACK,
          'file' => 'entdispfieldui.admin.inc',
        ]
        + $access;
    }
  }

  return $items;
}

/**
 * @param string $machine_name
 * @param string $entity_type
 * @param string $bundle_name
 * @param string|int $bundle_pos
 * @param string $view_mode_candidate
 *   Could be the view mode, or an arbitrary string.
 * @param array $map
 *
 * @return object|false
 *
 * @see field_group_menu_load()
 */
function entdispfieldui_field_load($machine_name, $entity_type, $bundle_name, $bundle_pos, $view_mode_candidate, $map) {

  if ($bundle_pos > 0) {
    $bundle = $map[$bundle_pos];
    $bundle_name = field_extract_bundle($entity_type, $bundle);
  }

  $entdispfieldui_field = _entdispfieldui_load_field(
    $machine_name,
    $entity_type,
    $bundle_name,
    $view_mode_candidate);

  if (NULL === $entdispfieldui_field) {
    // Create an incomplete object without an ->identifier key.
    $obj = new stdClass();
    $obj->entity_type = $entity_type;
    $obj->bundle_name = $bundle_name;
    $obj->view_mode = $view_mode_candidate;
    $obj->machine_name = $machine_name;
    return $obj;
  }

  // @todo This looks redundant.
  return (NULL === $entdispfieldui_field)
    ? new stdClass()
    : $entdispfieldui_field;
}
