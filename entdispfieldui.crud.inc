<?php

/**
 * @param string $machine_name
 * @param string $entity_type
 * @param string $bundle_name
 * @param string $view_mode
 *
 * @return bool
 *   TRUE, if a entry already exists.
 *
 * @see field_group_exists()
 */
function _entdispfieldui_field_exists($machine_name, $entity_type, $bundle_name, $view_mode) {
  $entdispfieldui_fields = _entdispfieldui_read_fields();
  return !empty($entdispfieldui_fields[$entity_type][$bundle_name][$view_mode][$machine_name]);
}

/**
 * @param string $entity_type
 * @param string $bundle
 * @param string $view_mode
 * @param bool $reset
 *
 * @return object[]|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface[]
 *   Format: $[$machine_name] = $entdispfieldui_field
 */
function _entdispfieldui_viewmode_fields($entity_type, $bundle, $view_mode, $reset = FALSE) {

  $fields = _entdispfieldui_field_info_fields_all($reset);

  if (isset($fields[$entity_type][$bundle][$view_mode])) {
    return $fields[$entity_type][$bundle][$view_mode];
  }

  return [];
}

/**
 * @param bool $reset
 *
 * @return object[][][][]|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface[][][][]
 *   Format: $[$entity_type][$bundle][$view_mode][$machine_name] = $entdispfieldui_field
 */
function _entdispfieldui_field_info_fields_all($reset = FALSE) {
  static $entdispfieldui_fields_all = FALSE;

  if (FALSE === $entdispfieldui_fields_all || $reset) {
    if (!$reset && $cached = cache_get(ENTDISPFIELDUI_CID_FIELDS_ALL, 'cache_field')) {
      $entdispfieldui_fields_all = $cached->data;
    }
    else {
      $ctools_export_load_object = &drupal_static('ctools_export_load_object');
      $ctools_export_load_object_all = &drupal_static('ctools_export_load_object_all');
      unset($ctools_export_load_object[ENTDISPFIELDUI_TABLE_NAME]);
      unset($ctools_export_load_object_all[ENTDISPFIELDUI_TABLE_NAME]);
      $entdispfieldui_fields_all = _entdispfieldui_read_fields();
      cache_set(ENTDISPFIELDUI_CID_FIELDS_ALL, $entdispfieldui_fields_all, 'cache_field');
    }
  }

  return $entdispfieldui_fields_all;
}

/**
 * Read all entdisp fields.
 *
 * @param array $conditions
 *   Parameters for the query, as elements of the $conditions array.
 *   'entity_type' The name of the entity type.
 *   'bundle' The name of the bundle.
 *   'view_mode' The view mode.
 *
 * @param boolean $enabled
 *   Return enabled or disabled fields.
 *
 * @return object[][][][]|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface[][][][]
 *   Format: $[$entity_type][$bundle][$view_mode][$machine_name] = $entdispfieldui_field
 *
 * @see field_group_read_groups()
 */
function _entdispfieldui_read_fields(array $conditions = [], $enabled = TRUE) {

  ctools_include('export');

  if ([] === $conditions) {
    $records = ctools_export_load_object(ENTDISPFIELDUI_TABLE_NAME);
  }
  else {
    $records = ctools_export_load_object(ENTDISPFIELDUI_TABLE_NAME, 'conditions', $conditions);
  }

  $entdispfieldui_fields = [];

  foreach ($records as $record) {

    if ($enabled) {
      // Return only enabled fields.
      if (isset($record->disabled) && $record->disabled) {
        continue;
      }
    }
    else {
      // Return only disabled fields.
      if (isset($record->disabled) || !$record->disabled) {
        continue;
      }
    }

    $entdispfieldui_fields
    [$record->entity_type]  // Entity type
    [$record->bundle]  // Bundle name
    [$record->view_mode]  // View mode
    [$record->machine_name]  // Machine name.
      = $record;
  }

  # drupal_alter('entdispfieldui_info', $entdispfieldui_fields);

  return $entdispfieldui_fields;
}

/**
 * Loads an EntDisP field definition.
 *
 * @param string $machine_name
 *   The machine name of the EntDisP field to load.
 * @param string $entity_type
 * @param string $bundle_name
 * @param string $view_mode
 *
 * @return object|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface|null
 *
 * @see field_group_load_field_group()
 */
function _entdispfieldui_load_field($machine_name, $entity_type, $bundle_name, $view_mode) {
  return _entdispfieldui_load_field_by_conditions(
    [
      'machine_name' => $machine_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle_name,
      'view_mode' => $view_mode,
    ]
  );
}

/**
 * Ctools load callback to load EntDisP field by identifier.
 *
 * @param string $identifier
 *
 * @return object|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface|null
 */
function _entdispfieldui_load_field_by_identifier($identifier) {
  return _entdispfieldui_load_field_by_conditions(
    [
      'identifier' => $identifier,
    ]
  );
}

/**
 * @param string[] $conditions
 *
 * @return object|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface|null
 */
function _entdispfieldui_load_field_by_conditions(array $conditions) {
  ctools_include('export');
  $records = ctools_export_load_object(ENTDISPFIELDUI_TABLE_NAME, 'conditions', $conditions);
  return array_shift($records);
}

/**
 * Saves an EntDisP field definition.
 *
 * @param object|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface $entdispfieldui_field
 *   An EntDisP field definition.
 *
 * @return bool|int
 *
 * @see field_group_group_save()
 * @see ctools_export_crud_save()
 *   Called from here as $export['save callback'].
 * @see entdispfieldui_schema()
 *   Registered here as ['export']['save_callback'].
 */
function _entdispfieldui_field_save($entdispfieldui_field) {

  if (isset($entdispfieldui_field->export_type) && $entdispfieldui_field->export_type & EXPORT_IN_DATABASE) {
    // Existing record.
    $update = ['id'];
    # module_invoke_all('field_group_update_field_group', $entdispfieldui_field);
  }
  else {
    // New record.
    $update = [];
    $entdispfieldui_field->export_type = EXPORT_IN_DATABASE;
    # module_invoke_all('field_group_create_field_group', $entdispfieldui_field);
  }

  return drupal_write_record(ENTDISPFIELDUI_TABLE_NAME, $entdispfieldui_field, $update);
}

/**
 * Delete an EntDisP field.
 * This function is also called by ctools export when calls are
 * made through ctools_export_crud_delete().
 *
 * @param object|\Drupal\entdispfieldui\EntdispfielduiField\EntdispfielduiFieldInterface $entdispfieldui_field
 *   An EntDisP field definition.
 * @param bool $ctools_crud
 *  Is this function called by the ctools crud delete.
 */
function _entdispfieldui_field_export_delete($entdispfieldui_field, $ctools_crud = TRUE) {

  $query = db_delete(ENTDISPFIELDUI_TABLE_NAME);

  if (isset($entdispfieldui_field->identifier)) {
    $query->condition('identifier', $entdispfieldui_field->identifier);
    if (!$ctools_crud) {
      ctools_export_crud_disable(ENTDISPFIELDUI_TABLE_NAME, $entdispfieldui_field->identifier);
    }
  }
  elseif (isset($entdispfieldui_field->id)) {
    $query->condition('id', $entdispfieldui_field->id);
  }

  /** @noinspection IsEmptyFunctionUsageInspection */
  if (!empty($entdispfieldui_field->view_mode)) {
    $query->condition('view_mode', $entdispfieldui_field->view_mode);
  }

  $query->execute();

  cache_clear_all(ENTDISPFIELDUI_CID_FIELDS_ALL, 'cache_field');
  # module_invoke_all('field_group_delete_field_group', $entdispfieldui_field);
}
