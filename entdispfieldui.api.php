<?php

/**
 * Hook that allows e.g. features to define entdispfieldui fields.
 *
 * @return stdClass[]
 */
function hook_entdispfieldui_info() {
  $export = [];

  $entdispfieldui_field = new stdClass();
  $entdispfieldui_field->api_version = 1;
  $entdispfieldui_field->identifier = 'entdisp_image|node|bicycle|row';
  $entdispfieldui_field->machine_name = 'entdisp_image';
  $entdispfieldui_field->entity_type = 'node';
  $entdispfieldui_field->bundle = 'bicycle';
  $entdispfieldui_field->view_mode = 'row';
  $entdispfieldui_field->visible = TRUE;
  $entdispfieldui_field->weight = 1;
  $entdispfieldui_field->plugin_id = 'renderkit.processedEntityImage';
  $entdispfieldui_field->plugin_options = [
    0 => [
      'id' => 'renderkit.fallback',
      'options' => [
        0 => [
          'id' => 'renderkit.entityImageField',
          'options' => [
            0 => 'field_pictures',
          ],
        ],
        1 => [
          'id' => 'EXAMPLE.productImageFallback',
        ],
      ],
    ],
    1 => [
      'id' => 'renderkit.imageStyle',
      'options' => [
        0 => 'row',
      ],
    ],
  ];
  $export['entdisp_image|node|bicycle|row'] = $entdispfieldui_field;

  return $export;
}
